﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Gravity : MonoBehaviour
{

    //Settings
    public bool isStatic = false;
    public Vector3 velocity;
    public float mass = 1f;
    public const float gravConstant = 1f;

    public Gravity orbitCenter;
    //public Transform sun;

    //properties
    public Vector3 position { get { return transform.position; } set { transform.position = value; } }



    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        if (orbitCenter && !isStatic)
        {

            position += velocity * Time.deltaTime*2;

            float distFromCenter = (orbitCenter.position - position).magnitude;
            float forceMagnitutde = gravConstant * orbitCenter.mass * mass / distFromCenter * distFromCenter;

            Vector3 force = forceMagnitutde * (orbitCenter.position - this.position).normalized;

            Vector3 acceleration = force / mass;
            Vector3 deltaVelocity = acceleration * Time.deltaTime;

            velocity += deltaVelocity;

        }



    }

    private void OnDrawGizmos()
    {
        if (orbitCenter && !isStatic)
        {

            Gizmos.color = Color.red; //hypothenuse
            Gizmos.DrawLine(orbitCenter.position, this.position);
            float height = Vector3.Dot(this.position, orbitCenter.transform.up);
            // Vector3 proj = new Vector3(this.position.x, this.position.y - height, this.position.z);
            Vector3 proj = this.position - height * orbitCenter.transform.up;
            Gizmos.color = Color.blue; // normal
            Gizmos.DrawLine(this.position, proj);
            Gizmos.DrawWireSphere(proj, 0.1f);
            Gizmos.color = Color.green; // projection
            Gizmos.DrawLine(orbitCenter.position, proj);
        }

    }


}
